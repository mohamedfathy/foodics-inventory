# Foodics Inventory Management System

## Table of Contents

- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Usage](#usage)
- [Testing](#testing)
- [License](#license)

## Prerequisites

Before you begin, ensure you have the following installed:

- PHP (>= 7.4)
- Composer
- MySQL

## Installation


1. **Navigate to the project directory:**

    ```bash
    cd foodics
    ```

2. **Install PHP dependencies:**

    ```bash
    composer install
    ```

3. **Configure your database connection in the `.env` file:**

    ```env
    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=foodics
    DB_USERNAME=your-database-username
    DB_PASSWORD=your-database-password
    ```

4. **Generate the application key:**

    ```bash
    php artisan key:generate
    ```

5. **Run database migrations and seeders:**

    ```bash
    php artisan migrate --seed
    ```


## Usage

1. **To run the Laravel development server, execute the following command:**

    ```bash
    php artisan serve
    ```

2. **Import Postman collection and environment:**

- Open Postman and click "Import" to import two files from project root directory.

## Testing

1. **Run the PHPUnit tests:**

    ```bash
    php artisan test
    ```

## License
This project is licensed under the MIT License - see the LICENSE file for details.

## Notes

- An endpoint is provided to update the current stock of an ingredient. If the stock level exceeds a certain percentage of the default stock, the low_stock_notified flag is set to false.
- If the current stock of an ingredient falls below zero, an exception will be thrown, and the transaction for order creation will be reverted.
- If you wish to test the email functionality, update the email service provider in the .env file.
- Emails are sent via a queue, so ensure that the service worker is installed
- Screenshots of sent email is available in the "screenshots" directory.
