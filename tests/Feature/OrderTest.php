<?php

namespace Tests\Feature;

use App\Enums\Enums;
use App\Exceptions\APIException;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class OrderTest extends TestCase
{
    public function testOrderIsCreatedSuccessfully()
    {
        $productId = 1;
        $quantity = 2;
        $request = [
            "products" => [
                [
                    'product_id' => $productId,
                    'quantity' => $quantity,
                ]
            ]
        ];
        $response = $this->post('/api/orders', $request);
        // Assert that the record has created successfully.
        $response->assertStatus(201);

        // Assert that the database has the correct record in the 'order_details' table
        $this->assertDatabaseHas('order_details', [
            'product_id' => $productId,
            'quantity' => $quantity,
        ]);
    }


    public function testStockIsUpdatedSuccessfully()
    {
        $productId = 1;
        $quantity = 2;
        $product = Product::find($productId);
        $productIngredient = $product->productIngredients->first();

        $beforeIngredientStock = $productIngredient->ingredient->current_stock;

        $request = [
            "products" => [
                [
                    'product_id' => $productId,
                    'quantity' => $quantity,
                ]
            ]
        ];
        $response = $this->post('/api/orders', $request);
        $response->assertStatus(201);


        $productIngredient->refresh();
        $afterIngredientStock = $productIngredient->ingredient->current_stock;


        $beforeIngredientStock = $beforeIngredientStock - ($quantity * ($productIngredient->ingredient_quantity / Enums::UNIT_CONVERSION));
        $beforeIngredientStock = number_format($beforeIngredientStock, 2, '.', '');
        $afterIngredientStock = number_format($afterIngredientStock, 2, '.', '');
        $this->assertEquals($beforeIngredientStock, $afterIngredientStock);
    }

    public function testIngredientStockRunOut()
    {
        $productId = 1;
        $quantity = 2;
        $productIngredient = (Product::find($productId))->productIngredients->first();

        $Ingredient = $productIngredient->ingredient;
        $Ingredient->current_stock = 0.1;
        $Ingredient->save();

        $request = [
            "products" => [
                [
                    'product_id' => $productId,
                    'quantity' => $quantity,
                ]
            ]
        ];

        $this->assertThrows(
            fn () => $this->withoutExceptionHandling()->post('/api/orders', $request),
            APIException::class
        );

        $productIngredient->refresh();
        $afterIngredientStock = $productIngredient->ingredient->current_stock;

        $this->assertEquals($Ingredient->current_stock, $afterIngredientStock);
        //
        $Ingredient->current_stock = 20;
        $Ingredient->save();
    }
}
