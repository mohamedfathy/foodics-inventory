<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class OrderDetailFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'order_id' => fake()->randomElement(range(1,5)),
            'product_id' => fake()->randomElement(range(1,5)),
            'quantity' => fake()->randomNumber(1),
            'created_at' => fake()->dateTime()->format("Y-m-d H:i:s"),
            'updated_at' => fake()->dateTime()->format("Y-m-d H:i:s"),

        ];
    }
}
