<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => fake()->randomElement(['Burger', 'Salad', 'Meat']),
            'description' => fake()->randomElement(['Burger Description', 'Salad Description', 'Meat Description']),
            'price' => fake()->randomFloat(10, 8,2),
            'is_available' => fake()->boolean,
            'created_at' => fake()->dateTime()->format("Y-m-d H:i:s"),
            'updated_at' => fake()->dateTime()->format("Y-m-d H:i:s"),

        ];
    }
}
