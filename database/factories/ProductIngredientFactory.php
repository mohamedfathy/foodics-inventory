<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class ProductIngredientFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'product_id' => fake()->randomElement(range(1,5)),
            'ingredient_id' => fake()->randomElement(range(1,5)),
            'ingredient_quantity' => fake()->randomFloat(10, 8,2),
            'ingredient_quantity_unit' => fake()->randomElement(['g']),
            'created_at' => fake()->dateTime()->format("Y-m-d H:i:s"),
            'updated_at' => fake()->dateTime()->format("Y-m-d H:i:s"),

        ];
    }
}
