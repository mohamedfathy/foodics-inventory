<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class IngredientFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => fake()->randomElement(['Beef', 'Cheese', 'Onion']),
            'description' => fake()->randomElement(['Beef description', 'Cheese description', 'Onion description']),
            'current_stock' => fake()->randomElement(range(1,10)),
            'stock_unit' => fake()->randomElement(['kg']),
            'default_stock' =>fake()->randomElement(range(1,10)),
            'low_stock_notified' => fake()->boolean,
            'created_at' => fake()->dateTime()->format("Y-m-d H:i:s"),
            'updated_at' => fake()->dateTime()->format("Y-m-d H:i:s"),

        ];
    }
}
