<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Ingredient;
use Illuminate\Support\Facades\DB;

class IngredientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $sql = "
        INSERT INTO ingredients
        (id, name, description, current_stock, stock_unit, default_stock, low_stock_notified, created_at, updated_at)
        VALUES(1, 'Beef', 'Beef description', 20.00, 'kg', 10.00, 0, '2024-01-02 12:00:00', '2024-01-02 12:00:00'),
        (2, 'Cheese', 'Cheese description', 5.00, 'kg', 2.50, 0, '2024-01-02 12:00:00', '2024-01-02 12:00:00'),
        (3, 'Onion', 'Onion description', 1.00, 'kg', 0.50, 0, '2024-01-02 12:00:00', '2024-01-02 12:00:00');
        ";
        DB::insert($sql);


        //Ingredient::factory()->count(10)->create();
    }
}
