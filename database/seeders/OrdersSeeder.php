<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Order;
use Illuminate\Support\Facades\DB;

class OrdersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $sql = "
        INSERT INTO orders
        (id, user_id, created_at, updated_at)
        VALUES(1, 1, '2024-01-02 12:00:00', '2024-01-02 12:00:00'),
        (2, 2, '2024-01-02 12:00:00', '2024-01-02 12:00:00'),
        (3, 3, '2024-01-02 12:00:00', '2024-01-02 12:00:00');
        ";
        DB::insert($sql);


        //Order::factory()->count(10)->create();
    }
}
