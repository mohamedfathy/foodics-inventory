<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\OrderDetail;
use Illuminate\Support\Facades\DB;

class OrderDetailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $sql = "
        INSERT INTO order_details
        (id, order_id, product_id, quantity, created_at, updated_at)
        VALUES(1, 1, 1, 2, '2024-01-02 12:00:00', '2024-01-02 12:00:00'),
        (2, 1, 2, 1, '2024-01-02 12:00:00', '2024-01-02 12:00:00');
        ";
        DB::insert($sql);


        //OrderDetail::factory()->count(10)->create();
    }
}
