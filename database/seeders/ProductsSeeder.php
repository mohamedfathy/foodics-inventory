<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Product;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $sql = "
        INSERT INTO products
        (id, name, description, price, is_available, created_at, updated_at)
        VALUES(1, 'Burger', 'Burger Description', 55.00, 1, '2024-01-02 12:00:00', '2024-01-02 12:00:00'),
        (2, 'Meat', 'Meat Description', 75.00, 1, '2024-01-02 12:00:00', '2024-01-02 12:00:00'),
        (3, 'Salad', 'Salad Description', 15.00, 1, '2024-01-02 12:00:00', '2024-01-02 12:00:00');
        ";
        DB::insert($sql);


        //Product::factory()->count(10)->create();
    }
}
