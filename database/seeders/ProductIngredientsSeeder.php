<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\ProductIngredient;
use Illuminate\Support\Facades\DB;

class ProductIngredientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $sql = "
        INSERT INTO product_ingredients
        (id, product_id, ingredient_id, ingredient_quantity, ingredient_quantity_unit, created_at, updated_at)
        VALUES(1, 1, 1, 150.00, 'g', '2024-01-02 12:00:00', '2024-01-02 12:00:00'),
        (2, 1, 2, 30.00, 'g', '2024-01-02 12:00:00', '2024-01-02 12:00:00'),
        (3, 1, 3, 20.00, 'g', '2024-01-02 12:00:00', '2024-01-02 12:00:00');
        ";
        DB::insert($sql);


        //ProductIngredient::factory()->count(10)->create();
    }
}
