<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Model;


class ProductIngredient extends Model
{
    use HasFactory;

    public $timestamps = true;

    protected $fillable = [
        'id',
        'product_id',
        'ingredient_id',
        'ingredient_quantity',
        'ingredient_quantity_unit',
    ];


    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }


    public function ingredient(): BelongsTo
    {
        return $this->belongsTo(Ingredient::class);
    }


}
