<?php

namespace App\Services;

use App\Enums\Enums;
use App\Repositories\IngredientsRepository;
use Illuminate\Database\Eloquent\Model;

class IngredientsService
{

    private IngredientsRepository $ingredientsRepository;

    public function __construct(IngredientsRepository $ingredientsRepository)
    {
        $this->ingredientsRepository = $ingredientsRepository;
    }


    public function update(array $params, $ingredient = null): Model
    {
        $ingredient = $ingredient ?? $this->ingredientsRepository->find($params);
        if (isset($params['new_stock'])) {
            $params = $this->handleNewStock($params, $ingredient);
        }

        return $this->ingredientsRepository->update($params, $ingredient);
    }

    public function handleNewStock($params, $ingredient): array
    {
        $params['current_stock'] = $ingredient->current_stock + $params['new_stock'];
        // If the sum of the added stock and the current inventory stock exceeds a certain
        // percentage of the default stock, the value of low_stock_notified should be set to false.
        if ($params['current_stock'] > ($ingredient->default_stock * Enums::LOW_STOCK_ALERT_LEVEL)) {
            $params['low_stock_notified'] = false;
        }
        return $params;
    }


}
