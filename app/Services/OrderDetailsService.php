<?php

namespace App\Services;

use App\Repositories\OrderDetailsRepository;
use Illuminate\Database\Eloquent\Model;

class OrderDetailsService
{
    private OrderDetailsRepository $orderDetailsRepository;

    public function __construct(
        OrderDetailsRepository $orderDetailsRepository,
    ) {
        $this->orderDetailsRepository = $orderDetailsRepository;
    }



    public function store(array $params): Model
    {
        return $this->orderDetailsRepository->store($params);
    }




}
