<?php

namespace App\Services;

use App\Enums\Enums;
use App\Exceptions\APIException;
use App\Mail\IngredientLowStockAlert;
use App\Repositories\OrdersRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class OrdersService
{
    private OrdersRepository $ordersRepository;
    private OrderDetailsService $orderDetailsService;
    private IngredientsService $ingredientsService;

    public function __construct(
        OrdersRepository $ordersRepository,
        OrderDetailsService $orderDetailsService,
        IngredientsService $ingredientsService,
    ) {
        $this->ordersRepository = $ordersRepository;
        $this->orderDetailsService = $orderDetailsService;
        $this->ingredientsService = $ingredientsService;
    }


    public function store(array $params): Model
    {
        return DB::transaction(function () use ($params) {
            // The user ID should be dynamically replaced with the ID of the currently authenticated user (Auth::user()->id).
            // However, as authentication is not within the scope of this task,
            // it is temporarily hardcoded for demonstration purposes.
            $params['user_id'] = 1;

            $order = $this->ordersRepository->store($params);

            foreach ($params['products'] as $item) {
                $item['order_id'] = $order->id;
                $orderDetail = $this->orderDetailsService->store($item);

                $this->handleUpdateStock($orderDetail);
            }

            return $order;
        });
    }

    // When this logic becomes too complex, it should be delegated to handlers within the service layer.
    /**
     * @throws APIException
     */
    public function handleUpdateStock($orderDetail): void
    {
        foreach ($orderDetail->product->productIngredients as $productIngredient) {
            $ingredient = $productIngredient->ingredient;
            $currentStock = $ingredient->current_stock - $orderDetail->quantity * ($productIngredient->ingredient_quantity / Enums::UNIT_CONVERSION);

            $ingredient = $this->ingredientsService->update([
                'current_stock' => $currentStock
            ], $ingredient);

            // If an exception is thrown during the order creation transaction, it will trigger a rollback of all database transactions.
            // This ensures that no negative values will be persisted in the database.
            if ($ingredient->current_stock < 0) {
                throw new APIException(__('Order creation failed. Insufficient stock for one or more items.'));
            }

            // If the current stock is below a certain percentage of the default stock and
            // the low_stock_notified flag has not been triggered, we should email the merchant regarding
            // the ingredient and update the low_stock_notified flag.
            if ($ingredient->current_stock < Enums::LOW_STOCK_ALERT_LEVEL * $ingredient->default_stock && !$ingredient->low_stock_notified) {
                $this->sendMerchantEmail($ingredient);
                $this->ingredientsService->update([
                    'low_stock_notified' => true
                ], $ingredient);
            }
        }
    }

    private function sendMerchantEmail($ingredient): void
    {
        // send and email to the merchant.
        $merchantEmail = 'torbedosquadron8@gmail.com';
        Mail::to($merchantEmail)->queue(new IngredientLowStockAlert($ingredient));
    }




}
