<?php

namespace App\Http\Resources\Base;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductIngredientsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'product_id' => $this->product_id,
            'ingredient_id' => $this->ingredient_id,
            'ingredient_quantity' => $this->ingredient_quantity,
            'ingredient_quantity_unit' => $this->ingredient_quantity_unit,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

        ];
    }
}
