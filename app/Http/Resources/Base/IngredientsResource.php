<?php

namespace App\Http\Resources\Base;

use Illuminate\Http\Resources\Json\JsonResource;

class IngredientsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'current_stock' => $this->current_stock,
            'current_stock_unit' => $this->current_stock_unit,
            'default_stock' => $this->default_stock,
            'low_stock_notified' => $this->low_stock_notified,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

        ];
    }
}
