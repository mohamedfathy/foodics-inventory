<?php

namespace App\Http\Controllers;

use App\Http\Resources\Base\IngredientsResource;
use App\Services\IngredientsService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class IngredientController extends Controller
{
    protected IngredientsService $ingredientsService;

    public function __construct(IngredientsService $ingredientsService)
    {
        $this->ingredientsService = $ingredientsService;
    }


    /**
     * update ingredients.
     *
     * @param Request $request
     * @param int|string $ingredientId
     * @return JsonResponse
     */
    public function update(Request $request, int|string $ingredientId): JsonResponse
    {
        $request->merge(['id' => $ingredientId]);
        $params = $request->validate([
            "id" => "required|int|exists:ingredients,id",
            "new_stock" => "nullable|numeric",
        ]);

        $response = $this->ingredientsService->update($params);
        return response()->json(new IngredientsResource($response));
    }

}
