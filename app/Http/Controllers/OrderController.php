<?php

namespace App\Http\Controllers;

use App\Http\Resources\Base\OrdersResource;
use Exception;
use App\Services\OrdersService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    protected OrdersService $ordersService;

    public function __construct(OrdersService $ordersService)
    {
        $this->ordersService = $ordersService;
    }


    /**
     * create order.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function store(Request $request): JsonResponse
    {
        $params = $request->validate([
            "products.*" => "required|array",
            "products.*.product_id" => "required|int|exists:products,id",
            "products.*.quantity" => "required|numeric",
        ]);

        $response = $this->ordersService->store($params);
        return response()->json(new OrdersResource($response), 201);
    }


}
