<?php

namespace App\Enums;

class Enums
{
    const UNIT_CONVERSION = 1000;
    const LOW_STOCK_ALERT_LEVEL = 0.5;

}
