<?php

namespace App\Repositories;

use App\Models\OrderDetail;

class OrderDetailsRepository extends DefaultRepository
{

    public mixed $model = OrderDetail::class;

    public function __construct()
    {
        parent::__construct($this->model);
    }
}