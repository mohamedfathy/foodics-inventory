<?php

namespace App\Repositories;

use App\Models\Order;

class OrdersRepository extends DefaultRepository
{
    public mixed $model = Order::class;

    public function __construct()
    {
        parent::__construct($this->model);
    }

}
