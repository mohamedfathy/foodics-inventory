<?php

namespace App\Repositories;

use App\Models\Ingredient;

class IngredientsRepository extends DefaultRepository
{

    public mixed $model = Ingredient::class;

    public function __construct()
    {
        parent::__construct($this->model);
    }
}