<?php

namespace App\Repositories;

use App\Models\ProductIngredient;

class ProductIngredientsRepository extends DefaultRepository
{

    public mixed $model = ProductIngredient::class;

    public function __construct()
    {
        parent::__construct($this->model);
    }
}