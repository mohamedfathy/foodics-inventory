<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

class DefaultRepository
{
    public mixed $model;

    public function __construct($model)
    {
        $this->model = new $model;
    }

    /**
     * find a record
     *
     * @param array $params
     * @return Model
     */
    public function find(array $params)
    {
        return $this->model::findOrFail($params['id']);
    }


    /**
     * store a record
     *
     * @param array $params
     * @return Model
     */
    public function store(array $params)
    {
        $model = new $this->model;
        $model->fill($params);
        $model->save();
        return $model;
    }


    /**
     * update a record
     *
     * @param array $params
     * @param Model|null $entity
     * @return Model
     */
    public function update(array $params, Model|null $entity = null):Model
    {
        if(!isset($entity)) {
            $entity = $this->model::findOrFail($params['id']);
        }
        $entity->fill($params);
        $entity->save();
        return $entity;
    }
}
