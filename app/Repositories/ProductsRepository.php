<?php

namespace App\Repositories;

use App\Models\Product;

class ProductsRepository extends DefaultRepository
{

    public mixed $model = Product::class;

    public function __construct()
    {
        parent::__construct($this->model);
    }
}