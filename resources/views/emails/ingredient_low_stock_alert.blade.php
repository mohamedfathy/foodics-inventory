<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ingredient Low Stock Alert</title>
</head>
<body>
<h2>Ingredient Low Stock Alert</h2>
<p>The stock of the ingredient "{{ $ingredient->name }}" has reached a low level (50% or below).</p>
<p>Please consider restocking this ingredient to avoid shortages.</p>
<p>Thank you for using our system.</p>
</body>
</html>
